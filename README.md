# quebec-housing

Looks for new houses and new prices available in a certain market.

Uses `selenium` to navigate `centris`.

## Requirements

- python 3.7+
- pip

## Local setup

### Install
```
bin/up
```

### Run
Fill a config file the way you want it the way `config.yml.example` is formatted.
```
python -m app.main config_file_path.yml <print|email>

bin/testrun  # prints out result in the console using test_config.yml
```

To use the `email` option, you must provide `HOTMAIL_USER` and `HOTMAIL_PASSWORD` environment variables.

### Format and lint

```
bin/verify
```

## FAQ
### Centris

Centris URL includes location and property type which makes search easy.  

##### New prices
```
<div class="shell">
  <div class="banner new-price"> # this implies a new price
    ...
  </div>
  <a class="property-thumbnail-summary-link" href="/link/we/care/about">
    ...
  </a>
</div>
```

##### New inscription
Note: The link is the same as in any shell
```
<div class="shell">
  <div class="banner new-property"> # this is a new property in the market
    ...
  </div>
  <a class="property-thumbnail-summary-link" href="/link/we/care/about">
    ...
  </a>
</div>
```

##### Navigate
```
<ul class="pager" role="navigation">
  ...
  <li class="next">
    <a></a> # click on me
  </li>
</ul>
```

### duProprio
`duProprio` makes search very easy as pretty much all filters are query params in the URL. There's also a
`posted less than 2 days ago` filter which makes finding new properties very straightforward.
