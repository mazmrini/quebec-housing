import time
from typing import Callable, List, Optional

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from .house_result import HouseResult
from .realtor import (FailedToFindPageContentError, NoMorePropertiesError,
                      Realtor)


class CentrisRealtor(Realtor):
    _TIMEOUT = 10
    _HOUSE_CARD_CSS = "div.shell"
    _NEW_PROPERTY_BANNER_CSS = "div.banner.new-property"
    _NEW_PRICE_BANNER_CSS = "div.banner.new-price"
    _HOUSE_RESULT_COUNT_CSS = "p.results-nb span.resultCount"
    _HOUSE_CARD_PRICE_CSS = "div.description div.price meta[itemprop=price][content]"
    _HOUSE_CARD_HREF_CSS = "a.property-thumbnail-summary-link[href]"
    _HOUSE_CARD_HREF_IMAGE_CSS = "img[itemprop=image][src]"
    _NEXT_PAGE_BUTTON_CSS = "ul.pager[role=navigation] li.next a"
    _INACTIVE_NEXT_PAGE_BUTTON_CSS = "ul.pager[role=navigation] li.next.inactive a"
    _ACCEPT_PRIVACY_PROMPT_BUTTON_CSS = "didomi-notice-agree-button"

    def __init__(
        self,
        get_driver: Callable[[], WebDriver],
        meta: str,
        landing_page: str,
        max_price: int,
    ) -> None:
        self._get_driver = get_driver
        self._driver: Optional[WebDriver] = None
        self._meta = meta
        self._landing_page = landing_page
        self._current_page = 0
        self._max_price = max_price

    def meta(self) -> str:
        return self._meta

    def __enter__(self) -> Realtor:
        self._driver = self._get_driver()
        self._driver.get(self._landing_page)
        time.sleep(2)  # gives time to the async server side code to load\
        self._bypass_privacy_prompt()
        time.sleep(1)
        return self

    def find_on_page(self) -> List[HouseResult]:
        if self._driver is None:
            raise FailedToFindPageContentError()

        time.sleep(3)  # gives time to the async server side code to load
        self._current_page += 1
        print(f"Looking on page {self._current_page} of {self._meta}")
        self._wait_for_house_cards_with_link_tags(self._driver)

        house_cards = self._driver.find_elements(By.CSS_SELECTOR, self._HOUSE_CARD_CSS)
        houses = []
        for house_card in house_cards:
            result = self._to_house_result(house_card)
            if result is not None and result.price_at_most(self._max_price):
                houses.append(result)

        return houses

    def go_to_next_page(self) -> None:
        if self._driver is None:
            raise FailedToFindPageContentError()

        self._wait_for_go_to_next_page()
        inactive_buttons = self._driver.find_elements(
            By.CSS_SELECTOR, self._INACTIVE_NEXT_PAGE_BUTTON_CSS
        )
        if len(inactive_buttons) > 0:
            raise NoMorePropertiesError()

        next_buttons = self._driver.find_elements(
            By.CSS_SELECTOR, self._NEXT_PAGE_BUTTON_CSS
        )
        if len(next_buttons) == 0:
            raise NoMorePropertiesError()

        next_buttons[0].click()

    def __exit__(self, exc_type, exc_val, exc_tb) -> bool:
        if exc_type is not None:
            print(exc_type)

        if exc_val is not None:
            print(exc_val)

        if self._driver is not None:
            self._driver.quit()
        return True

    def _wait_for_house_cards_with_link_tags(self, d: WebDriver) -> None:
        try:
            WebDriverWait(self._driver, self._TIMEOUT).until(
                EC.presence_of_element_located(
                    [By.CSS_SELECTOR, self._by_house_with_link_tag()]
                )
            )
        except TimeoutException:
            # Count loading is async. It only makes it reliable when no properties were found by the wait
            result_count = d.find_elements(
                By.CSS_SELECTOR, self._HOUSE_RESULT_COUNT_CSS
            )
            if (
                len(result_count) > 0
                and result_count[0].get_property("textContent") == "0"
            ):
                raise NoMorePropertiesError()
            raise FailedToFindPageContentError()

    def _wait_for_go_to_next_page(self) -> None:
        try:
            WebDriverWait(self._driver, self._TIMEOUT).until(
                EC.presence_of_element_located(
                    [By.CSS_SELECTOR, self._NEXT_PAGE_BUTTON_CSS]
                )
            )
        except TimeoutException:
            raise FailedToFindPageContentError()

    def _by_house_with_link_tag(self) -> str:
        return f"{self._HOUSE_CARD_CSS} {self._HOUSE_CARD_HREF_CSS}"

    def _to_house_result(self, house_card: WebElement) -> Optional[HouseResult]:
        a_tags = house_card.find_elements(By.CSS_SELECTOR, self._HOUSE_CARD_HREF_CSS)
        if len(a_tags) == 0:
            return None

        link_element = a_tags[0]
        link = link_element.get_attribute("href")
        is_new_property = (
            len(
                house_card.find_elements(By.CSS_SELECTOR, self._NEW_PROPERTY_BANNER_CSS)
            )
            > 0
        )
        if is_new_property:
            return HouseResult.new_house(
                link, self._find_price(house_card), self._find_image(house_card)
            )

        is_new_price = (
            len(house_card.find_elements(By.CSS_SELECTOR, self._NEW_PRICE_BANNER_CSS))
            > 0
        )
        if is_new_price:
            return HouseResult.new_price(
                link, self._find_price(house_card), self._find_image(house_card)
            )

        return None

    def _find_price(self, house_card: WebElement) -> Optional[int]:
        prices = house_card.find_elements(By.CSS_SELECTOR, self._HOUSE_CARD_PRICE_CSS)
        if len(prices) == 0:
            return None

        price = str(prices[0].get_attribute("content"))
        if not price.isnumeric():
            return None

        return int(price)

    def _find_image(self, house_link: WebElement) -> str:
        images = house_link.find_elements(
            By.CSS_SELECTOR, self._HOUSE_CARD_HREF_IMAGE_CSS
        )
        if len(images) == 0:
            return ""

        return images[0].get_attribute("src")

    def _bypass_privacy_prompt(self):
        accept_button = self._driver.find_element(By.ID, self._ACCEPT_PRIVACY_PROMPT_BUTTON_CSS)
        if accept_button:
            accept_button.click()
