from typing import List, Optional

from pydantic import BaseModel


class HouseResult(BaseModel):
    description: str
    link: str
    price: Optional[int]
    image_link: str

    @classmethod
    def error(cls) -> "HouseResult":
        return HouseResult(description="Something failed :(", link="Sorry")

    @classmethod
    def new_house(
        cls, link: str, price: Optional[int], image_link: str
    ) -> "HouseResult":
        return HouseResult(
            description="Nouvelle propriété",
            link=link,
            price=price,
            image_link=image_link,
        )

    @classmethod
    def new_price(
        cls, link: str, price: Optional[int], image_link: str
    ) -> "HouseResult":
        return HouseResult(
            description="Nouveau prix", link=link, price=price, image_link=image_link
        )

    def price_at_most(self, number: int) -> bool:
        if self.price is None:
            return False

        return self.price <= number

    def price_as_str(self) -> str:
        if self.price is None:
            return "?"

        return "{:,} $".format(self.price).replace(",", " ")


class HouseResults(BaseModel):
    meta: str
    houses: List[HouseResult]
