from typing import List

from .house_result import HouseResult, HouseResults
from .realtor import (FailedToFindPageContentError, NoMorePropertiesError,
                      Realtor)


class BotAgent:
    def __init__(self, realtors: List[Realtor]) -> None:
        self._realtors = realtors

    def search(self) -> List[HouseResults]:
        return [
            HouseResults(meta=r.meta(), houses=self._search_on_realtor(r))
            for r in self._realtors
        ]

    def _search_on_realtor(self, realtor: Realtor) -> List[HouseResult]:
        results = []
        looking = True
        with realtor as r:
            while looking:
                try:
                    results += r.find_on_page()
                    r.go_to_next_page()
                except NoMorePropertiesError:
                    looking = False
                except FailedToFindPageContentError:
                    print("Failed to find page content")
                    looking = False
                    results.append(HouseResult.error())

        return results
