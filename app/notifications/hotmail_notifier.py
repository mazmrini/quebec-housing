import datetime as dt
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import List

from ..realtors.house_result import HouseResult, HouseResults
from .notifier import Notifier


class HotmailNotifier(Notifier):
    _MONTHS = [
        "Janvier",
        "Février",
        "Mars",
        "Avril",
        "Mai",
        "Juin",
        "Juillet",
        "Août",
        "Septembre",
        "Octobre",
        "Novembre",
        "Décembre",
    ]

    def __init__(self, user: str, password: str, recipients: List[str]) -> None:
        self._user = user
        self._password = password
        self._recipients = recipients

    def send(self, results: List[HouseResults]) -> None:
        now = dt.datetime.now()
        nb_findings = sum([len(r.houses) for r in results])
        to = ", ".join(self._recipients)

        email_message = MIMEMultipart("alternative")  # enables html content
        email_message["From"] = self._user
        email_message["To"] = to
        email_message[
            "Subject"
        ] = f"{nb_findings} maisons - {now.day} {self._MONTHS[now.month - 1]} {now.year}"
        email_message.attach(self._html_attachment(results))

        smtp = None
        try:
            smtp = smtplib.SMTP("smtp-mail.outlook.com", 587)
            smtp.starttls()
            smtp.login(self._user, self._password)
            smtp.sendmail(self._user, self._recipients, email_message.as_string())
            print("Email sent!")
        except:  # noqa: E722
            print("Failed to send email :(")
        finally:
            if smtp is not None:
                smtp.quit()

    def _html_attachment(self, results: List[HouseResults]) -> MIMEText:
        body = "<br>".join([self._to_house_results_html(r) for r in results])
        html = "<html>" "<head></head>" "<body>" f"{body}" "</body>" "</html>"

        return MIMEText(html, "html")

    def _to_house_results_html(self, result: HouseResults) -> str:
        heading = f"<h3>{result.meta}</h3>"
        content = "<div>Rien de nouveau :(</div><br>"
        if len(result.houses) > 0:
            heading = f"<h3>{result.meta} ({len(result.houses)})</h3>"
            content = "<br>".join(
                [self._to_house_result_html(h) for h in result.houses]
            )

        return f"{heading}{content}"

    def _to_house_result_html(self, house_result: HouseResult) -> str:
        return (
            f"<div>"
            f"<div>{house_result.description} - {house_result.price_as_str()}</div>"
            f'<div><a href="{house_result.link}">'
            f'<img src="{house_result.image_link}" style="width: 200px">'
            f"</a></div>"
            f"</div>"
        )
