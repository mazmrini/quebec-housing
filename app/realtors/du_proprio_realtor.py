import time
from typing import Callable, List, Optional

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver, WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from .house_result import HouseResult
from .realtor import (FailedToFindPageContentError, NoMorePropertiesError,
                      Realtor)


class DuProprioRealtor(Realtor):
    _TIMEOUT = 10
    _NBSP = "\xa0"
    _NO_PROPERTIES_ON_PAGE_CSS = "h2.search-results-listings-no-results-found__title"
    _HOUSE_CARD_CSS = "li.search-results-listings-list__item"
    _HOUSE_CARD_HREF_CSS = "a.search-results-listings-list__item-image-link[property=significantLink][href]"
    _HOUSE_CARD_HREF_IMAGE_CSS = "img[ref][src]"
    _HOUSE_CARD_PRICE_CSS = "div.search-results-listings-list__item-description__price"
    _NEXT_PAGE_BUTTON_CSS = (
        "nav.pagination div.pagination__arrow.pagination__arrow--right"
    )
    _INACTIVE_NEXT_PAGE_BUTTON_CSS = (
        "nav.pagination div.pagination__arrow"
        ".pagination__arrow--right.pagination__arrow--disabled"
    )

    def __init__(
        self, get_driver: Callable[[], WebDriver], meta: str, landing_page: str
    ) -> None:
        self._get_driver = get_driver
        self._driver: Optional[WebDriver] = None
        self._meta = meta
        self._landing_page = landing_page
        self._current_page = 0

    def meta(self) -> str:
        return self._meta

    def __enter__(self) -> Realtor:
        self._driver = self._get_driver()
        self._driver.get(self._landing_page)
        time.sleep(3)  # gives time to the async server side code to load
        return self

    def find_on_page(self) -> List[HouseResult]:
        if self._driver is None:
            raise FailedToFindPageContentError()

        time.sleep(3)  # gives time to the async server side code to load
        self._current_page += 1
        print(f"Looking on page {self._current_page} of {self._meta}")

        self._wait_for_page_load()
        nothing_on_page = self._driver.find_elements(
            By.CSS_SELECTOR, self._NO_PROPERTIES_ON_PAGE_CSS
        )
        if len(nothing_on_page) > 0:
            raise NoMorePropertiesError()

        house_cards = self._driver.find_elements(By.CSS_SELECTOR, self._HOUSE_CARD_CSS)
        houses = [self._to_house_result(h) for h in house_cards]

        return [h for h in houses if h is not None]

    def go_to_next_page(self) -> None:
        if self._driver is None:
            raise FailedToFindPageContentError()

        self._wait_for_go_to_next_page()

        inactive_buttons = self._driver.find_elements(
            By.CSS_SELECTOR, self._INACTIVE_NEXT_PAGE_BUTTON_CSS
        )
        if len(inactive_buttons) > 0:
            raise NoMorePropertiesError()

        # nav element is not always clickable, fortunately, we can use the URL to change page
        next_page_url = self._driver.current_url.replace(
            f"pageNumber={self._current_page}", f"pageNumber={self._current_page + 1}"
        )
        self._driver.get(next_page_url)

    def __exit__(self, exc_type, exc_val, exc_tb) -> bool:
        if exc_type is not None:
            print(exc_type)

        if exc_val is not None:
            print(exc_val)

        if self._driver is not None:
            self._driver.quit()
        return True

    def _wait_for_page_load(self) -> None:
        def page_loaded(d: WebDriver) -> bool:
            empty = d.find_elements(By.CSS_SELECTOR, self._NO_PROPERTIES_ON_PAGE_CSS)
            houses = d.find_elements(By.CSS_SELECTOR, self._HOUSE_CARD_CSS)

            return (len(empty) + len(houses)) > 0

        try:
            WebDriverWait(self._driver, self._TIMEOUT).until(page_loaded)
        except TimeoutException:
            raise FailedToFindPageContentError()

    def _wait_for_go_to_next_page(self) -> None:
        try:
            WebDriverWait(self._driver, self._TIMEOUT).until(
                EC.presence_of_element_located(
                    [By.CSS_SELECTOR, self._NEXT_PAGE_BUTTON_CSS]
                )
            )
        except TimeoutException:
            raise FailedToFindPageContentError()

    def _to_house_result(self, house_card: WebElement) -> Optional[HouseResult]:
        a_tags = house_card.find_elements(By.CSS_SELECTOR, self._HOUSE_CARD_HREF_CSS)
        if len(a_tags) == 0:
            return None

        link_element = a_tags[0]
        link = link_element.get_attribute("href")

        images = link_element.find_elements(
            By.CSS_SELECTOR, self._HOUSE_CARD_HREF_IMAGE_CSS
        )
        image = ""
        if len(images) > 0:
            image = images[0].get_attribute("src")

        return HouseResult.new_house(link, self._find_price(house_card), image)

    def _find_price(self, house_card: WebElement) -> Optional[int]:
        prices = house_card.find_elements(By.CSS_SELECTOR, self._HOUSE_CARD_PRICE_CSS)
        if len(prices) == 0:
            return None

        price = (
            str(prices[0].get_property("textContent"))
            .strip()
            .replace(self._NBSP, "")
            .replace(" ", "")
            .replace("$", "")
            .replace(",", "")
        )
        if not price.isnumeric():
            return None

        return int(price)
