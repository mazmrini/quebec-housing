FROM joyzoursky/python-chromedriver:latest

COPY . .

RUN pip install -r requirements.txt

ENTRYPOINT ["python", "-m", "app.main", "test_config.yml", "print"]
