from typing import List, Optional

from pydantic import BaseModel, BaseSettings


class HotmailSettings(BaseSettings):
    hotmail_user: str
    hotmail_password: str


class UrlConfig(BaseModel):
    meta: str
    url: str
    max_price: Optional[int] = None


class AppConfig(BaseModel):
    recipients: List[str] = []
    centris: List[UrlConfig] = []
    du_proprio: List[UrlConfig] = []
