from typing import List

from ..realtors.house_result import HouseResult, HouseResults
from .notifier import Notifier


class ConsoleNotifier(Notifier):
    def send(self, results: List[HouseResults]) -> None:
        messages = []
        for r in results:
            messages.append("---")
            messages.append(r.meta)
            messages += [self._format(h) for h in r.houses]

        print("\n".join(messages))

    def _format(self, house_result: HouseResult) -> str:
        return f"{house_result.description} ({house_result.price_as_str()}) - {house_result.link}"
