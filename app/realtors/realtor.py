import abc
from typing import List

from .house_result import HouseResult


class Realtor(abc.ABC):
    @abc.abstractmethod
    def __enter__(self) -> "Realtor":
        ...

    @abc.abstractmethod
    def meta(self) -> str:
        ...

    @abc.abstractmethod
    def find_on_page(self) -> List[HouseResult]:
        ...

    @abc.abstractmethod
    def go_to_next_page(self) -> None:
        ...

    @abc.abstractmethod
    def __exit__(self, exc_type, exc_val, exc_tb) -> bool:
        ...


class NoMorePropertiesError(Exception):
    pass


class FailedToFindPageContentError(Exception):
    pass
